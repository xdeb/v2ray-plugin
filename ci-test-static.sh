#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


# TEST: run binary file (x86_64)
./dist/static/amd64/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"

# TEST: run binary file (x86)
./dist/static/386/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"

# TEST: install .apk package (x86_64)
apk add --allow-untrusted ./dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86_64.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"

# TEST: uninstall .apk package (x86_64)
apk del --no-network --rdepends $PKG_NAME
test ! -x /usr/bin/$PKG_NAME

# TEST: install .apk package (x86)
apk add --allow-untrusted ./dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"

# TEST: uninstall .apk package (x86_64)
apk del --no-network --rdepends $PKG_NAME
test ! -x /usr/bin/$PKG_NAME

# TEST: upgrade .apk package (x86_64)
apk add --allow-untrusted ./test_data/${PKG_NAME}_1.3.1-r1_x86_64.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v1.3.1"
apk add --allow-untrusted ./dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86_64.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"
apk del $PKG_NAME

# TEST: upgrade .apk package (x86)
apk add --allow-untrusted ./test_data/${PKG_NAME}_1.3.1-r1_x86.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v1.3.1"
apk add --allow-untrusted ./dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86.apk
/usr/bin/$PKG_NAME -version | grep "^$PKG_NAME v$PKG_VERSION"
apk del $PKG_NAME
