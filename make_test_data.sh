#!/bin/bash

. ./make_test_data.env
PS4='\n\[\033[1;92m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'

set -eux

srcdir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
pwd=$(pwd)
volname_cache=cache-$PKG_NAME


##############################
##                          ##
##  Caching `nfpm` command  ##
##                          ##
##############################

# Local cached `nfpm` command can speed up process
test -e /tmp/nfpm || wget --no-verbose -O- https://xdeb.gitlab.io/nfpm/nfpm-static_amd64.tar.gz | tar -C /tmp -xzf-


####################################
##                                ##
##  CI JOB: download-source-code  ##
##                                ##
####################################

tarball_name=$PKG_NAME-$PKG_VERSION.tar.gz

# Local cached source code tarball file can speed up process
test -e /tmp/$tarball_name || wget -O /tmp/$tarball_name https://github.com/shadowsocks/v2ray-plugin/archive/refs/tags/v${PKG_VERSION}.tar.gz


############################################
##                                        ##
##  CI JOB: build-bookworm-based-dynamic  ##
##                                        ##
############################################

ctname=$PKG_NAME-builder-bookworm

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache:/cache \
    -v /tmp/$tarball_name:$CI_PROJECT_DIR/$tarball_name:ro \
    -v /tmp/nfpm:/bin/nfpm:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file ./make_test_data.env \
    -e GOCACHE=/cache/go-build \
    -e GOMODCACHE=/cache/go-mod \
    -e DEBIAN_FRONTEND=noninteractive \
    golang:$TARGET_GOLANG_VERSION-bookworm
docker exec -it $ctname tar -xf $tarball_name

# Use local comstomized apt.conf to speed up `apt-get install`, if posible
test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

# Use local comstomized goproxy settings to speed up downloading
test -n "$(go env GOPROXY)" && docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm.yaml

# Do build
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/$PKG_NAME dist/*.deb'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


############################################
##                                        ##
##  CI JOB: build-bullseye-based-dynamic  ##
##                                        ##
############################################

ctname=$PKG_NAME-builder-bullseye

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache:/cache \
    -v /tmp/$tarball_name:$CI_PROJECT_DIR/$tarball_name:ro \
    -v /tmp/nfpm:/bin/nfpm:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file ./make_test_data.env \
    -e GOCACHE=/cache/go-build \
    -e GOMODCACHE=/cache/go-mod \
    -e DEBIAN_FRONTEND=noninteractive \
    golang:$TARGET_GOLANG_VERSION-bullseye
docker exec -it $ctname tar -xf $tarball_name

# Use local comstomized apt.conf to speed up `apt-get install`, if posible
test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

# Use local comstomized goproxy settings to speed up downloading
test -n "$(go env GOPROXY)" && docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm.yaml

# Do build
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/$PKG_NAME dist/*.deb'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


############################
##                        ##
##  CI JOB: build-static  ##
##                        ##
############################

ctname=$PKG_NAME-builder-static

# Set up builder container
docker rm -f $ctname
docker run --name=$ctname -d -t \
    -v $volname_cache:/cache \
    -v /tmp/$tarball_name:$CI_PROJECT_DIR/$tarball_name:ro \
    -v /tmp/nfpm:/bin/nfpm:ro \
    -w $CI_PROJECT_DIR \
    -h $ctname \
    --env-file ./make_test_data.env \
    golang:$TARGET_GOLANG_VERSION-alpine
docker exec -it $ctname sed -i s/https:/http:/ /etc/apk/repositories
docker exec -it $ctname apk --no-cache add bash
docker exec -it $ctname tar -xf $tarball_name

# Use local comstomized goproxy settings to speed up downloading
test -n "$(go env GOPROXY)" && docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
set +u
test -n "$GOPROXY" &&
    # $GOPROXY is higher than $(go env GOPROXY) in priority
    docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
set -u

# Simulate `git clone`: Copy all source files from the repository into the container.
sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

# patching nfpm.yaml and static-nfpm.yaml to avoid signature
docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm-static.yaml

# Do build
docker exec -it $ctname apk add git
docker exec -it $ctname ./ci-build.sh

# Gain artifacts
docker exec -it $ctname bash -c 'tar -czf artifacts.tar.gz dist/*/{amd64,386}/$PKG_NAME dist/*.{deb,apk}'
docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz


################
##            ##
##  DO TESTS  ##
##            ##
################

for codename in bullseye bookworm focal jammy noble
do
    case $codename in
        bullseye|bookworm) os=debian ;;
        focal|jammy|noble) os=ubuntu ;;
    esac

    docker run --rm -it \
        -v $pwd/ci-test.sh:/bin/ci-test.sh:ro \
        -v /tmp/${PKG_NAME}-builder-bullseye_artifacts.tar.gz:/tmp/bullseye_artifacts.tar.gz:ro \
        -v /tmp/${PKG_NAME}-builder-bookworm_artifacts.tar.gz:/tmp/bookworm_artifacts.tar.gz:ro \
        -v /tmp/${PKG_NAME}-builder-static_artifacts.tar.gz:/tmp/static_artifacts.tar.gz:ro \
        -v $pwd/test_data:/test_data:ro \
        -h test-$codename \
        --env-file ./make_test_data.env \
        -e CODENAME=$codename \
        -e DEBIAN_FRONTEND=noninteractive \
        $os:$codename sh -c "set -eux
        tar -xf /tmp/bullseye_artifacts.tar.gz
        tar -xf /tmp/bookworm_artifacts.tar.gz
        tar -xf /tmp/static_artifacts.tar.gz
        ls -lAF --color /dist/
        ci-test.sh"
done

docker run --rm -it \
    -v $pwd/ci-test-static.sh:/bin/ci-test-static.sh:ro \
    -v /tmp/${PKG_NAME}-builder-static_artifacts.tar.gz:/tmp/static_artifacts.tar.gz:ro \
    -v $pwd/test_data:/test_data:ro \
    -h test-alpine \
    --env-file ./make_test_data.env \
    alpine:latest sh -c "set -eux
    tar -xf /tmp/static_artifacts.tar.gz
    sed -i s/https:/http:/ /etc/apk/repositories
    apk add --no-cache bash
    ci-test-static.sh"


################################
##                            ##
##  COPY OUT .DEB, .APK FILE  ##
##                            ##
################################

mkdir -p test_data
rm -fv test_data/*.deb test_data/*.apk

tmpdir=$(mktemp -d)
tar -C $tmpdir -xf /tmp/${PKG_NAME}-builder-bullseye_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/${PKG_NAME}-builder-bookworm_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/${PKG_NAME}-builder-static_artifacts.tar.gz
cp -v $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bullseye_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~focal_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bookworm_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~jammy_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~noble_amd64.deb \
    $tmpdir/dist/${PKG_NAME}-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86_64.apk \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-r${PKG_RELEASE}_x86.apk \
    test_data/
rm -rf $tmpdir
